exports.getUserPhones = async function (connection, userId) {
    return new Promise((resolve, reject) => {
        connection.query('select * from `phones` where `author` = ?', [userId], (err, result, fields) => {
            if (!err) {
                resolve(JSON.stringify(result))
            } else {
                reject(err)
            }
        })
    })
}

exports.authUser = async function (connection, userInfo) {
    return new Promise((resolve, reject) => {
        const sql = 'select count(*) as count from `users` where `login` = ? and `password` = ?'
        connection.query(sql, [userInfo.login, userInfo.password], (err, result, fields) => {
            if (!err) {
                resolve(result[0].count)
            } else {
                reject(err)
            }
        })
    })
}

exports.updateContact = async function (connection, contact, userId) {
    return new Promise((resolve, reject) => {
        const sql = 'update `phones` set `firstname`=?,`lastname`=?,`email`=?,`phone`=? where `author` = ? and `id` = ?'
        connection.query(sql, [contact.firstname, contact.lastname, contact.email, contact.phone, userId, contact.id], (err, result, fields) => {
            if (!err) {
                resolve(result)
            } else {
                reject(err)
            }
        })
    })
}

exports.getContact = async function (connection, contactId, userId) {
    return new Promise((resolve, reject) => {
        const sql = 'select * from phones where id = ? and author = ?'
        connection.query(sql, [contactId, userId], (err, result, fields) => {
            if (!err) {
                resolve(result)
            } else {
                reject(err)
            }
        })
    })
}

exports.createContact = async function (connection, userId, contact) {
    return new Promise((resolve, reject) => {
        const sql = 'insert into `phones`(`author`,`firstname`,`lastname`,`email`,`phone`) values(?,?,?,?,?)'
        connection.query(sql, [userId, contact.firstname, contact.lastname, contact.email, contact.phone], (err, result, fields) => {
            if (!err) {
                resolve(result)
            } else {
                reject(err)
            }
        })
    })
}

exports.deleteContact = async function (connection, contactId, userId) {
    return new Promise((resolve, reject) => {
        const sql = 'delete from `phones` where `id` = ? and `author` = ?'
        connection.query(sql, [contactId, userId], (err, result, fields) => {
            if (!err) {
                resolve(result.affectedRows)
            } else {
                reject(err)
            }
        })
    })
}

exports.signup = async function (connection, user) {
    return new Promise((resolve, reject) => {
        const sql = 'insert into `users`(`login`,`password`) values(?,?)'
        connection.query(sql, [user.login, user.password], (err, result, fields) => {
            if (!err) {
                resolve(result)
            } else {
                reject(err)
            }
        })
    })
}

exports.deleteMultiple = async function (connection, userId, ids) {
    return new Promise((resolve, reject) => {
        const sql = 'delete from `phones` where `author` = ? and `id` in (?)'
        connection.query(sql, [userId, ids], (err, result, fields) => {
            if (!err) {
                resolve(result)
            } else {
                reject(err)
            }
        })
    })
}

exports.createSession = async function (connection, userId, sessionId) {
    return new Promise((resolve, reject) => {
        const sql = "insert into `sessions`(`sessionId`,`userId`) values(?,?)"
        connection.query(sql, [sessionId, userId], (err, result, fields) => {
            if (!err) {
                resolve(result)
            } else {
                reject(err)
            }
        })
    })
}

exports.findSession = async function (connection, sessionId) {
    return new Promise((resolve, reject) => {
        const sql = "select * from sessions where sessionId = ?"
        connection.query(sql, [sessionId], (err, result, fields) => {
            if (!err) {
                resolve(result)
            } else {
                reject(err)
            }
        })
    })
}

exports.getSessionUser = async function (connection, sessionId) {
    return new Promise((resolve, reject) => {
        const sql = "select userId from sessions where sessionId = ?"
        connection.query(sql, [sessionId], (err, result, fields) => {
            if (!err) {
                resolve(result)
            } else {
                reject(err)
            }
        })
    })
}

exports.deleteSession = async function (connection, sessionId) {
    return new Promise((resolve, reject) => {
        const sql = "delete from sessions where sessionId = ?"
        connection.query(sql, [sessionId], (err, result, fields) => {
            if (!err) {
                resolve(result)
            } else {
                reject(err)
            }
        })
    })
}