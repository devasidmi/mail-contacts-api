const crypto = require('crypto-js')
const Api = require('./Api.js')

exports.encryptHARD = function (value, secret) {
    return crypto.SHA256(value, secret).toString(crypto.enc.Base64)
}

exports.encrypt = function (value, secret) {
    return crypto.AES.encrypt(value, secret).toString()
}

exports.getUserId = async function (sessionId, connection, secret) {
    const sessionResult = await Api.getSessionUser(connection, sessionId)
    return crypto.AES.decrypt(sessionResult[0].userId, secret).toString(crypto.enc.Utf8)
}