const Koa = require('koa')
const Router = require('koa-router')
const cors = require('koa-cors');
const mysql = require('mysql')
const Api = require('./Api.js')
const Security = require('./Security.js')
const moment = require('moment')

const jsonHeader = "application/json"

const koaBody = require('koa-body');

const PORT = process.env.PORT || 3000
const PASSWORD_SECRET = process.env.PASSWORD_SECRET
const SESSION_SECRET = process.env.SESSION_SECRET

const mainCookie = "mailtask"
const origins = ["http://localhost:8080", "https://devasidmi.github.io"]

const dbConfig = {
    connectionLimit: 10,
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME
}

// const dbConfig = {
//   connectionLimit: 10,
//  host: 'localhost',
//  user: 'root',
//  password: 'root',
//  database: 'localdb',
//  port: 3001
// }

var connection;

function handleDisconnect() {

    try {
        connection = mysql.createPool(dbConfig);
    } catch (error) {
        setTimeout(handleDisconnect, 2000);
    }

    connection.on('error', function (err) {
        console.log('db error', err);
        if (err.code === 'PROTOCOL_CONNECTION_LOST') {
            handleDisconnect();
        } else {
            throw err;
        }
    });
}

handleDisconnect();

const app = new Koa();
const router = new Router();

async function isAuth(ctx) {
    const userCookie = ctx.cookies.get(mainCookie)
    if (userCookie) {
        const result = await Api.findSession(connection, userCookie)
        if (result.length > 0)
            return true
    }
    return false
}

async function createSession(login) {
    const userId = Security.encrypt(login, SESSION_SECRET)
    const sessionId = Security.encryptHARD(JSON.stringify({
        login: userId,
        created: moment().format()
    }), SESSION_SECRET)
    const result = await Api.createSession(connection, userId, sessionId)
    if (result.affectedRows > 0) {
        return sessionId
    }
    return null
}

function corsSetup(ctx) {
    const origin = ctx.request.header.origin
    if (origins.indexOf(origin) > -1) {
        ctx.res.setHeader("Access-Control-Allow-Origin", origin)
    }
    ctx.res.setHeader("Access-Control-Allow-Credentials", true)
}

router.get("/api/status", async (ctx, nex) => {
    ctx.res.type = jsonHeader
    corsSetup(ctx)
    ctx.body = {
        "message": "200"
    }
})

router.get('/api/phones/', async (ctx, next) => {
    ctx.type = jsonHeader
    corsSetup(ctx)
    if (await isAuth(ctx)) {
        const userId = await Security.getUserId(ctx.cookies.get(mainCookie), connection, SESSION_SECRET)
        const phones = await Api.getUserPhones(connection, userId)
        ctx.body = phones
        return
    }
    ctx.body = {
        "message": "you are not authorized"
    }
    ctx.res.statusCode = 401
})
router.post('/api/phones/delete', koaBody(), async (ctx, next) => {
    ctx.type = jsonHeader
    corsSetup(ctx)
    if (await isAuth(ctx)) {
        const ids = ctx.request.body
        const userId = await Security.getUserId(ctx.cookies.get(mainCookie), connection, SESSION_SECRET)
        const result = await Api.deleteMultiple(connection, userId, ids)
        if (result.affectedRows > 0) {
            ctx.body = {
                "message": "multiple contacts successfully deleted"
            }
            return
        }
        ctx.body = {
            "message": "can't delete contacts"
        }
        ctx.res.statusCode = 409
        return
    }
    ctx.body = {
        "message": "you are not authorized"
    }
    ctx.res.statusCode = 401
})

router.post('/api/phones/update', koaBody(), async (ctx, next) => {
    ctx.type = jsonHeader
    corsSetup(ctx)
    if (await isAuth(ctx)) {
        const userId = await Security.getUserId(ctx.cookies.get(mainCookie), connection, SESSION_SECRET)
        const contact = ctx.request.body
        const result = await Api.updateContact(connection, contact, userId)
        if (result.affectedRows > 0) {
            const updatedContact = await Api.getContact(connection, contact.id, userId)
            ctx.body = updatedContact[0]
            return
        }
        ctx.body = {
            "message": "can't update contact"
        }
        ctx.res.statusCode = 409
        return
    }
    ctx.body = {
        "message": "you are not authorized"
    }
    ctx.res.statusCode = 401
})

router.post('/api/phones/create', koaBody(), async (ctx, next) => {
    ctx.type = jsonHeader
    corsSetup(ctx)
    if (await isAuth(ctx)) {
        const userId = await Security.getUserId(ctx.cookies.get(mainCookie), connection, SESSION_SECRET)
        const contact = ctx.request.body
        const result = await Api.createContact(connection, userId, contact)
        if (result.affectedRows > 0) {
            ctx.body = {
                "message": "contact created successfully",
                "contactId": result.insertId
            }
            return
        }
        ctx.body = {
            "message": "can't create contact"
        }
        ctx.res.statusCode = 409
    }
    ctx.body = {
        "message": "you are not authorized"
    }
    ctx.res.statusCode = 401
})

router.delete('/api/phones/:id', koaBody(), async (ctx, next) => {
    ctx.type = jsonHeader
    corsSetup(ctx)
    if (await isAuth(ctx)) {
        const userId = await Security.getUserId(ctx.cookies.get(mainCookie), connection, SESSION_SECRET)
        const contactId = ctx.params.id
        const deletedRows = await Api.deleteContact(connection, contactId, userId)
        if (deletedRows > 0) {
            ctx.body = {
                "message": "contact successfully deleted"
            }
            return
        }
        ctx.body = {
            "message": `can't delete contact with id = ${contactId}`
        }
        ctx.res.statusCode = 409
        return
    }
    ctx.body = {
        "message": "you are not authorized"
    }
    ctx.res.statusCode = 401

})

router.post('/api/signup', koaBody(), async (ctx, next) => {
    ctx.type = jsonHeader
    corsSetup(ctx)
    if (await isAuth(ctx)) {
        ctx.body = {
            "message": "to signup, logout first"
        }
        ctx.res.statusCode = 409
        return
    }
    const user = ctx.request.body
    user.password = Security.encryptHARD(user.password, PASSWORD_SECRET)
    try {
        const result = await Api.signup(connection, user)
        if (result.affectedRows > 0) {
            ctx.body = {
                "message": "successfully signup"
            }
            return
        }
        ctx.body = {
            "message": "can't auth"
        }
        ctx.res.statusCode = 409
    } catch (error) {
        ctx.body = {
            "message": "can't auth"
        }
        ctx.res.statusCode = 409
    }
})

router.get('/api/user', async (ctx, next) => {
    ctx.type = jsonHeader
    corsSetup(ctx)
    if (!await isAuth(ctx)) {
        ctx.body = {
            "message": "you are not authorized"
        }
        ctx.res.statusCode = 409
        return
    }
    ctx.body = JSON.stringify({
        userId: await Security.getUserId(ctx.cookies.get(mainCookie), connection, SESSION_SECRET)
    })
})

router.post('/api/auth', koaBody(), async (ctx, next) => {
    ctx.type = jsonHeader
    corsSetup(ctx)
    userInfo = ctx.request.body
    userInfo.password = Security.encryptHARD(userInfo.password, PASSWORD_SECRET)
    if (await isAuth(ctx)) {
        ctx.body = {
            "message": "already authorized"
        }
        ctx.res.statusCode = 409
        return
    }
    const users = await Api.authUser(connection, userInfo)
    if (users > 0) {
        const sessionId = await createSession(userInfo.login)
        if (sessionId != null) {
            ctx.cookies.set(mainCookie, sessionId)
            ctx.body = {
                "message": "successfully authorized"
            }
            return
        } else {
            ctx.body = {
                "message": "Authorization error"
            }
            ctx.res.statusCode = 409
            return
        }
    }
    ctx.body = {
        "message": "Wrong login or password"
    }
    ctx.res.statusCode = 409
})

router.post('/api/logout', async (ctx, next) => {
    ctx.type = jsonHeader
    corsSetup(ctx)
    if (!await isAuth(ctx)) {
        ctx.body = {
            "message": "you are not authorized"
        }
        ctx.res.statusCode = 401
        return
    }
    const sessionId = ctx.cookies.get(mainCookie)
    const result = await Api.deleteSession(connection, sessionId)
    if (result.affectedRows > 0) {
        ctx.cookies.set(mainCookie, null)
        ctx.body = {
            "message": "successfully logout"
        }
        return
    }
    ctx.body = {
        "message:": "logout error"
    }
    ctx.res.statusCode = 409
})

const koaOptions = {
    origin: true,
    credentials: true
};

app
    .use(cors(koaOptions))
    .use(router.routes())
    .use(router.allowedMethods())

app.listen(PORT);
console.log(`Server is running on port: ${PORT}`)